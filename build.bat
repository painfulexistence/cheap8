echo off
title Cheap8 Builder

set PATH=C:\Program Files\mingw-w64\x86_64-8.1.0-posix-seh-rt_v6-rev0\mingw64\bin;%PATH%
echo Building...

gcc chip8.c ^
-w -Wl,-subsystem,console ^
-lmingw32 -lSDL2main -lSDL2 ^
-o cheap8.exe

if %errorlevel% == 0 (
  set /P run="Do you want to execute the output executable? (y/n) "
  if %run% == y (
    set /P rom="ROM: ./roms/ (default rom: INVADERS)"
    if %rom% == "" (set rom=INVADERS)
    echo Running... && cheap8.exe "roms/%rom%"
  )
)
echo Complete.
