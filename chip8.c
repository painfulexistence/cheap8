#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#ifdef _WIN32
  #include <Windows.h>
#else
  #include <unistd.h>
#endif
#include <SDL2/SDL.h>
//#define DEBUG 0
//Debug mode: (0) disassembly, (1) disassembly + reg dump

#define xline 64
#define yline 32
const int SCREEN_WIDTH = 512;
const int SCREEN_HEIGHT = 256;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Texture *texture = NULL;
uint32_t screen[xline * yline] = {0};
uint8_t keymap[16] = {
  SDL_SCANCODE_X,
  SDL_SCANCODE_1,
  SDL_SCANCODE_2,
  SDL_SCANCODE_3,
  SDL_SCANCODE_Q,
  SDL_SCANCODE_W,
  SDL_SCANCODE_E,
  SDL_SCANCODE_A,
  SDL_SCANCODE_S,
  SDL_SCANCODE_D,
  SDL_SCANCODE_Z,
  SDL_SCANCODE_C,
  SDL_SCANCODE_4,
  SDL_SCANCODE_R,
  SDL_SCANCODE_F,
  SDL_SCANCODE_V
};
uint8_t fontset[80] = {
  0xf0, 0x90, 0x90, 0x90, 0xf0,
  0x20, 0x60, 0x20, 0x20, 0x70,
  0xf0, 0x10, 0xf0, 0x80, 0xf0,
  0xf0, 0x10, 0xf0, 0x10, 0xf0,
  0x90, 0x90, 0xf0, 0x10, 0x10,
  0xf0, 0x80, 0xf0, 0x10, 0xf0,
  0xf0, 0x80, 0xf0, 0x90, 0xf0,
  0xf0, 0x10, 0x20, 0x40, 0x40,
  0xf0, 0x90, 0xf0, 0x90, 0xf0,
  0xf0, 0x90, 0xf0, 0x10, 0xf0,
  0xf0, 0x90, 0xf0, 0x90, 0x90,
  0xe0, 0x90, 0xe0, 0x90, 0xe0,
  0xf0, 0x80, 0x80, 0x80, 0xf0,
  0xe0, 0x90, 0x90, 0x90, 0xe0,
  0xf0, 0x80, 0xf0, 0x80, 0xf0,
  0xf0, 0x80, 0xf0, 0x80, 0x80
};
char *opcode[16] = {
  "cls/ret",
  "jump",
  "call",
  "sei",
  "snei",
  "ser",
  "movi",
  "addi",
  "bin",
  "sner",
  "imovi",
  "jump",
  "rnd",
  "dw",
  "wait",
  "miscellaneous"
};

typedef struct CPUState {
  uint8_t V[16]; //16 8-bit registers
  uint8_t delay;
  uint8_t sound;
  uint8_t draw;
  uint16_t I;
  uint16_t SP; //16 stack-frames
  uint16_t PC;
  uint8_t *memory; //4KB RAM
  uint8_t *pixels;
  uint8_t key[16];
} CPUState;

CPUState* CPUInit(void) {
  srand(time(NULL));
  CPUState *s = calloc(1, sizeof(CPUState));
  s->memory = malloc(0x1000);
  memset(s->memory, 0, 0x1000);
  s->pixels = calloc(2048, sizeof(uint8_t));
  memset(s->pixels, 0, 0x800);
  s->I = 0;
  s->PC = 0x200; //3744B(0x200-0xe9f)
  s->SP = 0xea0; //96B(0xea0-0xeff)
  s->delay = 0;
  s->sound = 0;
  s->draw = 0;
  for (int i = 0; i < 16; i++) {
    s->V[i] = 0;
    s->key[i] = 0;
  }
  for (int i = 0; i < 80; i++) {
    s->memory[i] = fontset[i];
  }
  return s;
}

int tick(CPUState *state) {
  uint8_t *code = &state->memory[state->PC];
  uint8_t nibble = (*code & 0xf0) >> 4;
  #ifdef DEBUG
    printf("%04x -> %02x %02x %s \n", state->PC, code[0], code[1], opcode[nibble]);
  #endif
  switch(nibble) {
    case 0x00:
      //clear screen or return
      {
        if (code[1] == 0xE0) {
          for (int i = 0; i < 2048; i++) {
            state->pixels[i] = 0;
          }
          state->draw = 1;
          state->PC += 2;
        } else if (code[1] == 0xEE) {
          state->SP -= 2;
          state->PC = ((uint16_t)state->memory[state->SP] << 8) + (uint16_t)state->memory[state->SP + 1];
          state->PC += 2;
        } else {
          printf("1802 not implemented");
          exit(1);
        }
      }
      break;
    case 0x01:
      //jump
      state->PC = ((uint16_t)(code[0] & 0x0f) << 8) + (uint16_t)(code[1]);
      break;
    case 0x02:
      //call
      state->memory[state->SP] = (uint8_t)((state->PC & 0xff00) >> 8);
      state->memory[state->SP + 1] = (uint8_t)(state->PC & 0x00ff);
      state->SP += 2;
      state->PC = ((uint16_t)(code[0] & 0x0f) << 8) + (uint16_t)(code[1]);
      break;
    case 0x03:
      //conditionally skip
      {
        if (state->V[code[0] & 0x0f] == code[1]) {
          state->PC += 2;
        }
        state->PC += 2;
      }
      break;
    case 0x04:
      //conditionally skip
      {
        if (state->V[code[0] & 0x0f] != code[1]) {
          state->PC += 2;
        }
        state->PC += 2;
      }
      break;
    case 0x05:
      //conditionally skip
      {
        if (state->V[code[0] & 0x0f] == state->V[(code[1] & 0xf0) >> 4]) {
          state->PC += 2;
        }
        state->PC += 2;
      }
      break;
    case 0x06:
      //load
      state->V[code[0] & 0x0f] = code[1];
      state->PC += 2;
      break;
    case 0x07:
      //add
      state->V[code[0] & 0x0f] += code[1];
      state->PC += 2;
      break;
    case 0x08:
      //bitwise operation
      {
        uint8_t x = code[0] & 0x0f;
        uint8_t y = (code[1] & 0xf0) >> 4;
        switch (code[1] & 0x0f) {
          case 0x00:
            state->V[x] = state->V[y];
            break;
          case 0x01:
            state->V[x] |= state->V[y];
            break;
          case 0x02:
            state->V[x] &= state->V[y];
            break;
          case 0x03:
            state->V[x] ^= state->V[y];
            break;
          case 0x04:
            state->V[0xf] = 0;
            if ((uint16_t)state->V[x] + (uint16_t)state->V[y] > 0x00ff) {
              state->V[0xf] = 1;
            }
            state->V[x] += state->V[y];
            break;
          case 0x05:
            state->V[0xf] = 0;
            if (state->V[x] > state->V[y]) state->V[0xf] = 1;
            state->V[x] -= state->V[y];
            break;
          case 0x06:
            state->V[0xf] = 0;
            if (state->V[x] & 0x01 != 0) state->V[0xf] = 1;
            state->V[x] = state->V[x] >> 1;
            break;
          case 0x07:
            state->V[0xf] = 0;
            if (state->V[x] < state->V[y]) state->V[0xf] = 1;
            state->V[x] = state->V[y] - state->V[x];
            break;
          case 0x0E:
            state->V[0xf] = 0;
            if (state->V[x] & 0x80 != 0) state->V[0xf] = 1;
            state->V[x] = state->V[x] << 1;
            break;
        }
        state->PC += 2;
      }
      break;
    case 0x09:
      //conditionally skip
      {
        if (state->V[code[0] & 0x0f] != state->V[(code[1] & 0xf0) >> 4]) {
          state->PC += 2;
        }
        state->PC += 2;
      }
      break;
    case 0x0A:
      //load I
      state->I = ((uint16_t)(code[0] & 0x0f) << 8) + (uint16_t)(code[1]);
      state->PC += 2;
      break;
    case 0x0B:
      //flow
      state->PC = ((uint16_t)(code[0] & 0x0f) << 8) + (uint16_t)(code[1]) + state->V[0];
      break;
    case 0x0C:
      //random
      state->V[code[0] & 0x0f] = rand() & code[1];
      state->PC += 2;
      break;
    case 0x0D:
      //draw sprite
      {
        uint8_t x = state->V[code[0] & 0x0f];
        uint8_t y = state->V[(code[1] & 0xf0) >> 4];
        uint8_t n = code[1] & 0x0f;

        state->V[0xf] = 0;
        for (int j = 0; j < n; j++) {
          for (int i = 0; i < 8; i++) {
            if (((state->memory[state->I + j] << i) & 0x80) == 0x80) {
              if (state->pixels[((x + i) % 0x40) + ((y + j) % 0x20) * 0x40] == 1) {
                state->V[0xf] = 1;
              }
              state->pixels[((x + i) % 0x40) + ((y + j) % 0x20) * 0x40] ^= 1;
            }
          }
        }
        state->draw = 1;
      }
      state->PC += 2;
      break;
    case 0x0E:
      //conditionally skip by key
      {
        if (code[1] == 0x9E && (state->key[state->V[code[0] & 0x0f]] != 0)) {
          state->PC += 2;
        }
        if (code[1] == 0xA1 && (state->key[state->V[code[0] & 0x0f]] == 0)) {
          state->PC += 2;
        }
        state->PC += 2;
      }
      break;
    case 0x0f:
      //miscellaneous
      switch (code[1]) {
        case 0x07:
          state->V[code[0] & 0x0f] = state->delay;
          state->PC += 2;
          break;
        case 0x0A:
          {
            SDL_Event e;
            SDL_WaitEvent(&e);
            if (e.type == SDL_KEYDOWN) {
              for (int i = 0; i < 16; i++) {
                if (e.key.keysym.scancode == keymap[i]) {
                  state->V[code[0] & 0x0f] = i;
                  state->PC += 2;
                }
              }
            }
          }
          break;
        case 0x15:
          state->delay = state->V[code[0] & 0x0f];
          state->PC += 2;
          break;
        case 0x18:
          state->sound = state->V[code[0] & 0x0f];
          state->PC += 2;
          break;
        case 0x1E:
          state->I = state->I + (uint16_t)(state->V[code[0] & 0x0f]);
          state->PC += 2;
          break;
        case 0x29:
          state->I = (uint16_t)(state->V[code[0] & 0x0f] * 5);
          state->PC += 2;
          break;
        case 0x33:
          {
            uint8_t x = state->V[code[0] & 0x0f];
            state->memory[state->I] = x / 100;
            state->memory[state->I + 1] = (x % 100) / 10;
            state->memory[state->I + 2] = x % 10;
          }
          state->PC += 2;
          break;
        case 0x55:
          for (int i = 0; i <= (code[0] & 0x0f); i++) {
            state->memory[state->I + i] = state->V[i];
          }
          state->PC += 2;
          break;
        case 0x65:
          for (int i = 0; i <= (code[0] & 0x0f); i++) {
            state->V[i] = state->memory[state->I + i];
          }
          state->PC += 2;
          break;
      }
      break;
  }
  return 1;
}

void loadROM(CPUState* state, char *filename) {
  FILE *f = fopen(filename, "rb");
  if (f == NULL) {
    printf("error: Couldn't open %s\n", filename);
    exit(1);
  }

  fseek(f, 0L, SEEK_END);
  int fsize = ftell(f); // Calculate file size in bytes
  fseek(f, 0L, SEEK_SET);

  uint8_t *buffer = &state->memory[0x200];
  fread(buffer, fsize, 1, f);
  fclose(f);
}

int main(int argc, char **argv) {
  CPUState *state = CPUInit();
  loadROM(state, argv[1]);
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    printf("SDL failed to initialize.\n");
    exit(1);
  }
  window = SDL_CreateWindow("Cheap8", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
  if (window == NULL) {
    printf("Window could not be created.\n");
    exit(1);
  }
  SDL_SetWindowResizable(window, SDL_TRUE);
  renderer = SDL_CreateRenderer(window, -1, 0);
  texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, xline, yline);

  SDL_Event e;
  int quit = 0;
  int ips = 2000;
  uint8_t clock = 0;
  while (quit != 1) {
    clock += tick(state);
    if (clock % (ips / 60) == 0) {
      if (state->delay > 0) state->delay--;
      if (state->sound > 0) state->sound--;
    }
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        quit = 1;
        break;
      }
    }
    uint8_t *keyboard = SDL_GetKeyboardState(NULL);
    for (int i = 0; i < 16; i++) {
      state->key[i] = keyboard[keymap[i]];
    }
    if (state->draw == 1) {
      state->draw = 0;
      for (int i = 0; i < xline * yline; i++) {
        uint8_t p = state->pixels[i]; //fetch graphics buffer
        screen[i] = (p * 0x00ffffff) | 0xff000000;
      }
      SDL_UpdateTexture(texture, NULL, screen, xline * sizeof(uint32_t));
      SDL_RenderClear(renderer);
      SDL_RenderCopy(renderer, texture, NULL, NULL);
      SDL_RenderPresent(renderer);
    }
    #if DEBUG == 1
      printf("I: %04x, ", state->I);
      for(int i = 0; i < 15; i++) {
        printf("V%d: %02x, ", i, state->V[i]);
      }
      printf("V15: %02x\n", state->V[15]);
    #endif
    #ifdef _WIN32
      Sleep(1000.0/(float)ips);
    #else
      usleep(1000000.0/(float)ips);
    #endif
  }
  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}
